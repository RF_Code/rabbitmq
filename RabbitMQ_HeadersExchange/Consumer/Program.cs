﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        channel.ExchangeDeclare(exchange: "headersexchange", type: ExchangeType.Headers);

        channel.QueueDeclare("letterbox1");

        var bindingArg = new Dictionary<string, object>()
        {
            {"x-match", "all" },
            {"name", "Brian" },
            {"age", "21" }
        };

        channel.QueueBind("letterbox1", "headersexchange", "", bindingArg);

        var consumer = new EventingBasicConsumer(channel);

        consumer.Received += (model, ea) =>
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Recieved new message: {message}");
        };

        channel.BasicConsume(queue: "letterbox1", autoAck: true, consumer: consumer);

        Console.WriteLine("Consuming");

        Console.ReadKey();
    }
}
