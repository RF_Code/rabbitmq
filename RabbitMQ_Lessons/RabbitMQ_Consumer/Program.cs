﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        channel.QueueDeclare(
            queue: "letterbox",
            durable: false,
            exclusive: false,
            autoDelete: false,
            arguments: null);

        channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false); // Competing Consumer

        var consumer = new EventingBasicConsumer(channel);
         
        consumer.Received += (model, ea) =>
        {
            var processingTime = new Random().Next(1,6);

            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Recived: {message} will take {processingTime} to process");
            Task.Delay(TimeSpan.FromSeconds(processingTime)).Wait();

            channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false); // Competing Consumer
        };

        //chanel.BasicConsume(queue: "letterbox", autoAck: true, consumer: consumer); 
        channel.BasicConsume(queue: "letterbox", autoAck: false, consumer: consumer); // Competing Consumer
        Console.ReadKey();
    }
}
