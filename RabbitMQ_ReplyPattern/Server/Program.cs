﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        channel.QueueDeclare(
            queue: "request_queue",
            exclusive: false);

        var consumer = new EventingBasicConsumer(channel);

        consumer.Received += (model, ea) =>
        {
            Console.WriteLine($"Recive request: {ea.BasicProperties.CorrelationId}");

            var replyMassage = $"This is reply message: {ea.BasicProperties.CorrelationId}";

            var body = Encoding.UTF8.GetBytes(replyMassage);

            channel.BasicPublish("", ea.BasicProperties.ReplyTo, null, body);
        };

        channel.BasicConsume(queue: "request_queue", autoAck: true, consumer: consumer);

        Console.ReadKey();
    }
}
