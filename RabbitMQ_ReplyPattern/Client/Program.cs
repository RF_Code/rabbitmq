﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        var replyQueue = channel.QueueDeclare(
            queue: "",
            exclusive: true);

        channel.QueueDeclare(
            queue: "request_queue",
            exclusive: false);

        var consumer = new EventingBasicConsumer(channel);

        consumer.Received += (model, ea) =>
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Reply Recived: {message}");
        };

        channel.BasicConsume(queue: replyQueue.QueueName, autoAck: true, consumer: consumer);

        var message = "Can I request a reply";
        var body = Encoding.UTF8.GetBytes(message);

        var properties = channel.CreateBasicProperties();
        properties.ReplyTo = replyQueue.QueueName;
        properties.CorrelationId = Guid.NewGuid().ToString();

        channel.BasicPublish("", "request_queue", properties, body);

        Console.WriteLine($"Sending request: {properties.CorrelationId}");

        Console.WriteLine("Started Client");

        Console.ReadKey();
    }
}
