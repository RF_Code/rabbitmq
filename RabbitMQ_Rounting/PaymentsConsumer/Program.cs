﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {

        //channel.ExchangeDeclare(exchange: "myroutingexchange", ExchangeType.Direct);
        channel.ExchangeDeclare(exchange: "mytopicexchange", ExchangeType.Topic);

        var queueName = channel.QueueDeclare().QueueName;

        //channel.QueueBind(queue: queueName, exchange: "myroutingexchange", routingKey: "paymentsonly");
        //channel.QueueBind(queue: queueName, exchange: "myroutingexchange", routingKey: "both");
        channel.QueueBind(queue: queueName, exchange: "mytopicexchange", routingKey: "#.payments");

        var consumer = new EventingBasicConsumer(channel);

        consumer.Received += (model, ea) =>
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Payments - Recived message: {message}");

        };

        channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

        Console.WriteLine("Payments - Consuming");
        Console.ReadKey();
    }
}

