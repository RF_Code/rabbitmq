﻿using RabbitMQ.Client;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        //channel.ExchangeDeclare(exchange: "myroutingexchange", type: ExchangeType.Direct);
        channel.ExchangeDeclare(exchange: "mytopicexchange", type: ExchangeType.Topic);

        var userPaymentMessage = $"A european user paid for something";

        var userPaymentBody = Encoding.UTF8.GetBytes(userPaymentMessage);

        channel.BasicPublish("mytopicexchange", "user.europe.payments", null, userPaymentBody);

        Console.WriteLine($"Published message: {userPaymentMessage}");

        var buisnessOrderedMessage = $"A european buisness ordered goods";

        var buisnessOrderedBody = Encoding.UTF8.GetBytes(buisnessOrderedMessage);

        channel.BasicPublish("mytopicexchange", "business.europe.order", null, buisnessOrderedBody);

        Console.WriteLine($"Published message: {buisnessOrderedMessage}");
    }
}
