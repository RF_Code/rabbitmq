﻿using RabbitMQ.Client;
using System.Text;

var factory = new ConnectionFactory { HostName = "localhost" };

using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        channel.ExchangeDeclare(exchange: "pubsub", type: ExchangeType.Fanout);

        var message = "Hallo there";

        var encodedMessage = Encoding.UTF8.GetBytes(message);

        channel.BasicPublish("pubsub", "", null, encodedMessage);

        Console.WriteLine($"Published message: {message}");
    }
}
